package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {
            if(inputNumbers.size()>=Integer.MAX_VALUE-1){
                throw new CannotBuildPyramidException();
                }
            Collections.sort(inputNumbers);
            int[] array = new int[inputNumbers.size()];
            Iterator<Integer> iterator = inputNumbers.iterator();
            int i = 0;
            while (iterator.hasNext()) {
                array[i] = Integer.parseInt(String.valueOf(iterator.next()));
                i++;
            }
            int[][] NewArray = fieldSize(array);
            createPiramid(NewArray, array);
            return NewArray;
        }
        catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }

    private static int[][] fieldSize(int[] arr) {
        int b = arr.length;
        int i = 1, h = 0, j;
        while (b > 0) {
            b-= i;
            i++;
            h++;
        }
        j = h * 2 - 1;
        if (b < 0 || (b == 0 && h < 2)) {
            throw new CannotBuildPyramidException();
        }
        return new int[h][j];
    }

    private static void createPiramid(int[][] arr2, int[] arr) {
        int start = 0, i = 0, j;
        for (int x = 0; x < arr2.length; x++) {
            if (x == 0) {
                start = arr2[0].length / 2;
                } else {
                start--;
                }
            j = x;
            for (int y = start; j >= 0; y += 2) {
                arr2[x][y] = arr[i];
                i++;
                j--;
            }
        }
    }
}